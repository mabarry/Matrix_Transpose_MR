# Transpose Matrix avec Map Reduce
Supposons `A` la matrice à transposer

Le mapper `MatrixTransposeMapper` : fonctionnement

Pour une ligne `A[i][1];A[i][2];A[i][3];...`

Pour la ligne `A[i:] -> [j, (i, A[i][j])]`

A la fin du mapping des données on obtient:

pour chaque clé `j` la liste `[(1, A[1][j]), (2, A[2][j]), (3, A[3][j]), ...]`

Le reducer `MatrixTransposeReducer` : fonctionnement

Construit une matrice `B` qui est:

Pour chaque clé `j` la ligne `j` de la `B` devient :

`B[j:]=A[1][j];A[2][j];A[3][j];...`

A la fin la matrice `B` est la transposée de `A`

La classe `PairWritable` représente le couple (i, A[i][j]) (la valeur de sortie du mapper)
