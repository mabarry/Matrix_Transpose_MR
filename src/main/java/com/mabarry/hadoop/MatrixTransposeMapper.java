package com.mabarry.hadoop;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MatrixTransposeMapper extends Mapper<LongWritable, Text, LongWritable, PairWritable> {

	private int lineNumber = 0;

	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString(); // i
		lineNumber++;
		StringTokenizer st = new StringTokenizer(line, ",;");
		int count = 0;
		LongWritable countKey = new LongWritable();
		while(st.hasMoreTokens()) {
			String token = st.nextToken();
			countKey.set(count++);
			context.write(countKey, PairWritable.of(lineNumber, token)); //
		}
	}
}