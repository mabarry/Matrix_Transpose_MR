package com.mabarry.hadoop;

import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MatrixTransposeReducer extends Reducer<LongWritable, PairWritable, LongWritable, Text> {

	@Override
	protected void reduce(LongWritable key, Iterable<PairWritable> values, Context context) throws IOException, InterruptedException {
		StringBuilder sb = new StringBuilder();
		boolean start = true;
		SortedSet<PairWritable> list = new TreeSet<>(); // Force sort
		for(PairWritable value : values) {
			list.add(value.get());
		}
		for(Pair value : list) {
			if(start) {
				sb.append(value);
				start = false;
			} else {
				sb.append(",").append(value);
			}
		}
		Text result = new Text();
		result.set(sb.toString());
		context.write(null, result);
	}
}