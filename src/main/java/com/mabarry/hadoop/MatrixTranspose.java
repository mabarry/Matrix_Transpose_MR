package com.mabarry.hadoop;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

public class MatrixTranspose extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new MatrixTranspose(), args);
        System.exit(exitCode);
    }

    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }
        Job job = new Job();
        job.setJarByClass(MatrixTranspose.class);
        job.setJobName("MatrixTranspose");

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(PairWritable.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setMapperClass(MatrixTransposeMapper.class);
        job.setReducerClass(MatrixTransposeReducer.class);

        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if (returnValue == 0) { // Success
            Files.copy(Paths.get(args[1], "part-r-00000"), Paths.get("out.csv"), StandardCopyOption.REPLACE_EXISTING);
        }
        System.out.println("job.isSuccessful " + job.isSuccessful());
        return returnValue;
    }
}