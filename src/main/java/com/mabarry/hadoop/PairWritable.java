package com.mabarry.hadoop;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class PairWritable extends Pair<Integer, String> implements WritableComparable<PairWritable> {

    public PairWritable() {
        this(0, "");
    }

    public PairWritable(Integer integer, String text) {
        super(integer, text);
    }

    @Override
    public int compareTo(PairWritable o) {
        return getA().compareTo(o.getA());
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(getA());
        String value = getB();
        if (value == null) {
            value = "";
        }
        int length = value.length();
        dataOutput.writeInt(length);
        dataOutput.writeBytes(value);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.a = dataInput.readInt();
        int length = dataInput.readInt();
        byte[] data = new byte[length];
        dataInput.readFully(data);
        this.b = new String(data);
    }

    public PairWritable get() {
        return new PairWritable(this.a, this.b);
    }

    public static PairWritable of(int lineNumber, String token) {
        return new PairWritable(lineNumber, token);
    }

    @Override
    public String toString() {
        return String.valueOf(b);
    }
}
